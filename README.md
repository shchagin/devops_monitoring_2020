### Monitoring example application using docker-compose

In order to launch:
```bash
 > cd project_folder
 > docker-compose build
 > docker-compose up
```

After launching, open `localhost:3000` in your browser. 
Log in grafana using login `'admin'` and password `'admin'`
Add datasource with type `Graphite` and server `http://graphite:80`
Add a dashboard, choosing query `graphite`, metric `shares` 


Wait for some time, your result should look like this:


![Example image](image.png)
Format: ![Alt Text](url)

![Example image](image2.png)
Format: ![Alt Text](url)